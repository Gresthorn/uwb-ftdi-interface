# UWB FTDI INTERFACE PROJECT

This project aims at creating a stable library that provides a simple interface for use FTDI driver (USB-RS232) for receiving and transmitting data. Library was established with the help of the already existing library D2XX. Our aim is to simplify connection establishment and it's maintenance as much as possible and reduce down the functions required for the most common FTDI manipulation so programmers can concentrate merely on data processing algorithms. At the same time, in relation to minimizing function calls, we expect the cleaner source code when it comes to initialization phase of FTDI devices concerned. The project is implemented purely in C++. However, we may extend it by Qt framework algorithms later, since our other projects are Qt based. We remind that the project is still at the stage of intensive development and deployment of libraries in own applications is the sole responsibility of the author.

### Project info
  - *Latest version:* 0.1 **(development release)**
  - *Date of release:* 18.10.2015 
  - *Authors:* Peter Mikula
  - *License:* MIT licence
  - *Contact:* mikula.ptr@gmail.com

### References
* [FTDI chip][FTDI-CHIP-OFFICIAL-WEBSITE] - Official website of FTDI chip
* [FTDI programmer's guide][FTDI-CHIP-OFFICIAL-PROGRAMMERS-GUIDE] - Official documentation to D2XX library

> NOTE: This project is very fresh and there is still much to do. I do not recommend to use this source code in any 
> until first production release will not be published. New suggestions and error reporting is welcome.

[FTDI-CHIP-OFFICIAL-WEBSITE]: <http://www.ftdichip.com/>
[FTDI-CHIP-OFFICIAL-PROGRAMMERS-GUIDE]: <http://www.ftdichip.com/Support/Documents/ProgramGuides/D2XX_Programmer's_Guide%28FT_000071%29.pdf>